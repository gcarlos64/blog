---
title: Contact
...

- Email: [gcarlos@disroot.org](mailto:gcarlos@disroot.org)
- Telegram: @gcarlos64
- Git
    - Codeberg: [https://codeberg.org/gcarlos](https://codeberg.org/gcarlos)
    - GitHub: [https://github.com/gcarlos64](https://github.com/gcarlos64)
